#! /bin/bash

if [ "$IFACE" == "uap0" ]; then
    uap0Dir="/sys/class/net/uap0"
    if [ ! -d "$uap0Dir" ]; then
        iw dev mlan0 interface add uap0 type __ap
    fi
    wpa_supplicant -B -Dnl80211 -iuap0 -c/etc/pegase-ap-wpa_supplicant.conf 
exit
fi
