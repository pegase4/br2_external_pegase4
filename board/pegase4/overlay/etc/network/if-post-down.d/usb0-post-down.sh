#! /bin/bash

if [ "$IFACE" == "usb0" ]; then
    # Arret du dhcp
    #Récupération du pid du dhcpd
    pid_dhcpd_usb0=$(ps aux | grep "dhcpd" | grep "usb0" | awk '{print $1}')
    # Commande pour arrêter l'application*
    kill -9 $pid_dhcpd_usb0

exit
fi
