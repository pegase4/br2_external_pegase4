#! /bin/bash

if [ "$IFACE" == "uap0" ]; then
    # Arret du dhcp
    #Récupération du pid du dhcpd
    pid_dhcpd_uap0=$(ps aux | grep "dhcpd" | grep "uap0" | awk '{print $1}')
    # Commande pour arrêter l'application*
    kill -9 $pid_dhcpd_uap0

    # Arret de wpa_supplicant
    # Récupération du pid de wpa_sup
    pid_wpa_sup_uap0=$(ps aux | grep "wpa_supplicant" | grep "uap0" | awk '{print $1}')
    # Commande pour arrêter l'application *
    kill -9 $pid_wpa_sup_uap0  
    iw dev uap0 del
exit
fi
