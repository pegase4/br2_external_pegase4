#! /bin/bash

if [ "$IFACE" == "usb0" ]; then
    touch /var/lib/dhcp/usb0.leases
    dhcpd -cf /etc/dhcp/dhcpd-usb0.conf -lf /var/lib/dhcp/usb0.leases -pf /var/run/dhcpd-usb0.pid usb0
exit
fi
