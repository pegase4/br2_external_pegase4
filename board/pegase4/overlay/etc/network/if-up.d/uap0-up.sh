#! /bin/bash

if [ "$IFACE" == "uap0" ]; then
    touch /var/lib/dhcp/uap0.leases
    dhcpd -cf /etc/dhcp/dhcpd-uap0.conf -lf /var/lib/dhcp/uap0.leases -pf /var/run/dhcpd-uap0.pid uap0
exit
fi
