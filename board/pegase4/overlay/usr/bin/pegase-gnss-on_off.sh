#!/bin/bash

STATE_FILE="/sys/bus/platform/drivers/reg-userspace-consumer/gnss-switch/state"

if [ "$1" == "1" ]; then
    echo "enabled" > "$STATE_FILE"
elif [ "$1" == "0" ]; then
    echo "disabled" > "$STATE_FILE"
else
    echo "Usage: $0 <1|0>"
    exit 1
fi

systemctl restart zeus-time
