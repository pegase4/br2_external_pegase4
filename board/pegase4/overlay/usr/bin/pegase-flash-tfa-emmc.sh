#!/bin/bash

TFA=/boot/tf-a-pegase4-tfa.stm32

if [ -f "$TFA" ]; then
    echo "Start Flashing TFA into emmc"
    echo 0 > /sys/class/block/mmcblk2boot0/force_ro
    if [ $? -ne 0 ]; then
        echo "Fail to unlock rw into part mmcblk2boot0"
        exit 1
    fi
    
    mmc bootbus set single_backward x1 x1 /dev/mmcblk2
    if [ $? -ne 0 ]; then
        echo "Fail to set single_backward into /dev/mmcblk2"
        exit 1
    fi
    
    dd if=/boot/tf-a-pegase4-tfa.stm32 of=/dev/mmcblk2boot0
    if [ $? -ne 0 ]; then
        echo "Fail to write TFA into /dev/mmcblk2boot0"
        exit 1
    fi
    
    mmc bootpart enable 1 1 /dev/mmcblk2
    if [ $? -ne 0 ]; then
        echo "Fail to enable bootpart into /dev/mmcblk2"
        exit 1
    fi

    # for fail-safe update
    echo 0 > /sys/class/block/mmcblk2boot1/force_ro
    if [ $? -ne 0 ]; then
        echo "Fail to unlock rw into part mmcblk2boot1"
        exit 1
    fi
    dd if=/boot/tf-a-pegase4-tfa.stm32 of=/dev/mmcblk2boot1
    if [ $? -ne 0 ]; then
        echo "Fail to write TFA into /dev/mmcblk2boot1"
        exit 1
    fi
    
    echo "End Flashing TFA into emmc Successful :)"
else
    echo "TFA not found (/boot/tf-a-pegase4-tfa.stm32)"
    exit 1
fi
