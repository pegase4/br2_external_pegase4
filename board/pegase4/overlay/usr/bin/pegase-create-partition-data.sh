#! /bin/sh



#boot on sdcard
cat /proc/cmdline | grep -q "4a801b3e-c2a3-4644-a09f-892de9cfdba9"
if test $? -eq  0 ; then
	dev="/dev/mmcblk1"
	nb_new_part=5
fi;
#boot on emmc
cat /proc/cmdline | grep -q "c463da36-b4fe-4c78-8c5a-c387407eb11d"
if test $? -eq  0 ; then
	dev="/dev/mmcblk2"
	nb_new_part=3
fi;

sgdisk -i $nb_new_part $dev | grep -q "Partition name: 'to_ext4'"
if test $? -eq  0 ; then
	echo "[PEGASE] creeation ext4"
	mkfs.ext4 -L data "${dev}p${nb_new_part}"
	sgdisk -c $nb_new_part:"data" $dev
	tmpDir=$(mktemp -d) 
	mount "${dev}p${nb_new_part}" $tmpDir
	cp -a /usr/lcpc_initial/* $tmpDir
	umount "${dev}p${nb_new_part}"
	rmdir $tmpDir
	mkdir /lcpc/
	chown -R pegase /lcpc/
fi;

sgdisk -i $nb_new_part $dev | grep -q "Partition name: 'data'"
if test $? -eq  0 ; then
	mount "${dev}p${nb_new_part}" /lcpc/
	exit 0
fi;

if test "$1" != "start"; then
	echo "##############################################" > /dev/console   
	echo "#####   Partition is not on max size     #####" > /dev/console
	echo "  run pegase-create-partition-data.sh start   " > /dev/console
	echo "##############################################" > /dev/console
	exit 0
fi;

sgdisk -i $nb_new_part $dev | grep -q "does not exist"
if test $? -eq  0 ; then
	echo "[PEGASE] creeation partition"
	sgdisk -e $dev
	sgdisk -N $nb_new_part $dev
	sgdisk -c $nb_new_part:"to_ext4" $dev
	reboot
	exit 0
fi;



