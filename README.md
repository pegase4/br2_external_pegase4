# Version de buildroot à utiliser
git clone https://github.com/buildroot/buildroot.git
buildroot tag : 2022.11
buildroot tag : 2023.11.1
# proxy
declare -x http_proxy="http://137.121.61.4:3128"

# Creer une images
mkdir build_folder
cd build_folder
make -C ../buildroot/ BR2_EXTERNAL=../br2_external_pegase4/ pegase4_defconfig O=$PWD
utiliser une des lignes suivantes :
- make 2>&1 | tee build.log
- ./utils/brmake

## utilisateurs
- root : mdp pegase
- pegase : mdp pegase

## tric
### uboot
- boot_instance = 0 (boot sd) = 1 (boot emmc)
- ums 0 0, monter la sd en par l'USB (dans uboot)
- ums 0 1, monter la emmc en par l'USB (dans uboot)
### booter en emmc
- pmic (une fois) : dans uboot, faire "run pmicprog"
- tfa (une fois) : dans linux faire "pegase-flash-tfa-emmc.sh"
- flasher partition : dans uboot, faire "ums 0 1"
- rebooter en mettant les switch pour boot emmc
#### Préparer une carte opti
- mettre carte sd (et gpio en boot sd)
- arreter le boot
	- run pmicprog
	- ums 0 1 (flasher emmc)
- boot en sd
- pegase-flash-tfa-emmc.sh
- boot en emmc (avec les gpio)
- pegase-create-partition-data.sh start

### autre
- allumer alim uc 3v3 (connecteur) : gpioset gpiochip5 12=1
- faire des linux-rebuild uboot-rebuild host-uboot-tools-rebuild (mkimage pour boot.scr)
- make linux-rebuild : refaire le dts
- make host-uboot-tools-rebuild :boot.rc


## NETWORMANAGER conf
nmcli connection add type wifi con-name sii_labo ifname mlan0 ssid "sii" 
nmcli connection modify mywifi wifi-sec.key-mgmt wpa-psk
nmcli connection modify mywifi wifi-sec.psk pegase123456789 
or 
nmcli connection add type wifi con-name sii_labo ifname mlan0 ssid sii wifi-sec.key-mgmt wpa-psk wifi-sec.psk pegase123456789

Mode AP : 
ifup uap0 (address de la carte 192.168.2.1)
ssid pegase-ap
passw pegase4life
## utiliser un overlay
Dans le fichier /boot/uEnv.txt :
	- mettre la variable overlay_is_ok=1
	- mettre le nom de l'overlay dans overlay_to_apply=nom-de-l-overlay (sans le .dtb), loverlay doit être dans le dossier /boot/
	
## compiler une application qt
/XXXXX/build_folder/host/bin/qmake -o Makefile src.pro
### installer qt
https://www.qt.io/offline-installers

## compiler un driver
Dans un makefile :  
$(MAKE) -C $(KERNEL_DIR) ARCH=$(KERNEL_ARCH) CROSS_COMPILE=$(KERNEL_CROSS_COMPILE) M=$(PWD) modules    
A la main :  
make -C /XXXXX/build_folder/build/linux-5.10.10 ARCH=arm CROSS_COMPILE=/XXXXX/build_folder/host/bin/arm-none-linux-gnueabihf- M=$(PWD) modules

## PINMUX
- pin mux page 93 : stm32mp157c.pdf

## Erreur quand activation de libpegase après
Le problème c'est que l'option 16-bit pcre2 n'est pas activé lors de la première compilation alors que qt5base en a besion
- faire make pcre2-dirclean
- faire make qt5base-dirclean
### enlever les printk
echo 0 > /proc/sys/kernel/printk