################################################################################
#
# pegase-m
#
################################################################################

PEGASE_M_VERSION     = v1.1
#PEGASE_M_SITE        = $(TOPDIR)/../../paquet/pegase-m
#PEGASE_M_SITE_METHOD = local
PEGASE_M_SITE        = git@gitlab.univ-eiffel.fr:pegase4/pegase-m.git
PEGASE_M_SITE_METHOD = git
PEGASE_M_DEPENDENCIES += libpegase

ifeq ($(BR2_PACKAGE_PEGASE_M_GPS),y)
PEGASE_M_CONF_OPTS += -Dpegase_m_gps=true
else
PEGASE_M_CONF_OPTS += -Dpegase_m_gps=false
endif

ifeq ($(BR2_PACKAGE_PEGASE_M_HF8CH),y)
PEGASE_M_CONF_OPTS += -Dpegase_m_hf8ch=true
else
PEGASE_M_CONF_OPTS += -Dpegase_m_hf8ch=false
endif

ifeq ($(BR2_PACKAGE_PEGASE_M_HOSTNAME),y)
PEGASE_M_CONF_OPTS += -Dpegase_m_hostname=true
else
PEGASE_M_CONF_OPTS += -Dpegase_m_hostname=false
endif

ifeq ($(BR2_PACKAGE_PEGASE_M_WIFI),y)
PEGASE_M_CONF_OPTS += -Dpegase_m_wifi=true
else
PEGASE_M_CONF_OPTS += -Dpegase_m_wifi=false
endif

$(eval $(meson-package))
