################################################################################
#
# MASCAROAD
#
################################################################################

MASCAROAD_VERSION     = v1.1
#MASCAROAD_SITE        = $(TOPDIR)/../projet_mascaroad_git_lab/copie_git
#MASCAROAD_SITE_METHOD = local
MASCAROAD_SITE        = git@gitlab.univ-eiffel.fr:geii/mascaroad.git
MASCAROAD_SITE_METHOD  = git
MASCAROAD_INSTALL_STAGING = YES

define MASCAROAD_BUILD_CMDS
    $(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D)
endef

define MASCAROAD_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/Mascaroad $(TARGET_DIR)/opt/
    $(INSTALL) -D -m 0755 $(@D)/boitier.jpg $(TARGET_DIR)/opt/
    $(INSTALL) -D -m 0755 $(@D)/logo.jpg $(TARGET_DIR)/opt/
     $(INSTALL) -D -m 0755 $(@D)/wait.gif $(TARGET_DIR)/opt/


  # Installer les répertoires css et js
    $(INSTALL) -d $(TARGET_DIR)/opt/css
    $(INSTALL) -d $(TARGET_DIR)/opt/js

    # Copier les fichiers du répertoire css
    $(INSTALL) -m 0755 $(@D)/css/* $(TARGET_DIR)/opt/css/

    # Copier les fichiers du répertoire js
    $(INSTALL) -m 0755 $(@D)/js/* $(TARGET_DIR)/opt/js/
    
    # Installer le fichier de service systemd
    $(INSTALL) -D -m 0644 $(@D)/mascaroad.service $(TARGET_DIR)/etc/systemd/system/mascaroad.service

endef

$(eval $(qmake-package)) 
