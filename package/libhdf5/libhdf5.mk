################################################################################
#
# libhdf5
#
################################################################################

LIBHDF5_VERSION = hdf5_1.14.4
LIBHDF5_SITE = $(call github,HDFGroup,hdf5,$(LIBHDF5_VERSION))

LIBHDF5_SUPPORTS_IN_SOURCE_BUILD = NO
LIBHDF5_CONF_OPTS += -DBUILD_SHARED_LIBS=ON

$(eval $(cmake-package))
$(eval $(host-cmake-package))
