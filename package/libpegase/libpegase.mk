################################################################################
#
# libpegase
#
################################################################################

LIBPEGASE_VERSION     = v1.3
#LIBPEGASE_SITE        = $(TOPDIR)/../../paquet/libpegase
#LIBPEGASE_SITE_METHOD = local
LIBPEGASE_SITE        = git@gitlab.univ-eiffel.fr:pegase4/libpegase.git
LIBPEGASE_SITE_METHOD  = git
LIBPEGASE_INSTALL_STAGING = YES
LIBPEGASE_DEPENDENCIES += pegase-driver
LIBPEGASE_DEPENDENCIES += libgpiod

$(eval $(qmake-package))
