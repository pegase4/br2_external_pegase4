################################################################################
#
# python-h5py
#
################################################################################

PYTHON_H5PY_VERSION = 3.12.1
PYTHON_H5PY_SITE = https://github.com/h5py/h5py/archive/refs/tags
PYTHON_H5PY_SOURCE = $(PYTHON_H5PY_VERSION).tar.gz

PYTHON_H5PY_SETUP_TYPE = setuptools

PYTHON_H5PY_DEPENDENCIES = \
    host-pkgconf \
    host-python-cython \
    host-python-numpy \
    host-python-pkgconfig \
    python3 \
    python-numpy \
    libhdf5 \
    host-libhdf5 \
    python-pkgconfig
    
    
    
# Fixation des variables pour éviter que python-h5py se construise pour x86 au lieu de ARM
PYTHON_H5PY_ENV = \
    TARGET_ARCH=$(BR2_ARCH) \
    CROSS_COMPILE=$(TARGET_CROSS) \
    STAGING_DIR=$(STAGING_DIR) \
    PKG_CONFIG_PATH=$(STAGING_DIR)/usr/lib/pkgconfig \
    CFLAGS="$(TARGET_CFLAGS) -I$(STAGING_DIR)/usr/include -I$(TARGET_DIR)/usr/include" \
    LDFLAGS="$(TARGET_LDFLAGS) -L$(STAGING_DIR)/usr/lib -L$(TARGET_DIR)/usr/lib -Wl,-rpath=$(TARGET_DIR)/usr/lib"

# Ajout d'options de compilation pour éviter les erreurs de plateforme guessing
PYTHON_H5PY_BUILD_OPTS = build_ext

# Correction du problème de configuration multi-chemins de HDF5
PYTHON_H5PY_INSTALL_TARGET_OPTS = $(PYTHON_H5PY_BUILD_OPTS)


$(eval $(python-package))
