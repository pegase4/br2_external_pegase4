################################################################################
#
# python-pkgconfig
#
################################################################################

PYTHON_PKGCONFIG_VERSION = 1.5.5
PYTHON_PKGCONFIG_SOURCE = pkgconfig-1.5.5.tar.gz
PYTHON_PKGCONFIG_SITE = https://files.pythonhosted.org/packages/c4/e0/e05fee8b5425db6f83237128742e7e5ef26219b687ab8f0d41ed0422125e
PYTHON_PKGCONFIG_SETUP_TYPE = setuptools
PYTHON_PKGCONFIG_LICENSE = MIT
PYTHON_PKGCONFIG_LICENSE_FILES = LICENSE

$(eval $(python-package))
$(eval $(host-python-package))

