################################################################################
#
# gt
#
################################################################################

GT_VERSION = 7f9c45d98425a27444e49606ce3cf375e6164e8e
GT_SITE = $(call github,linux-usb-gadgets,gt,$(GT_VERSION))
GT_LICENSE = Apache-2.0
GT_LICENSE_FILES = LICENSE
GT_SUBDIR="source"

$(eval $(cmake-package))
