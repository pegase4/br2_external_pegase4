################################################################################
#
# geronimo-api-module
#
################################################################################

GERONIMO_API_VERSION     = v1.5
#GERONIMO_API_SITE        = $(TOPDIR)/../../paquet/geronimo_api
#GERONIMO_API_SITE_METHOD = local
GERONIMO_API_SITE        = git@gitlab.univ-eiffel.fr:pegase4/geronimo_api.git
GERONIMO_API_SITE_METHOD = git
GERONIMO_API_INSTALL_TARGET = YES

GERONIMO_API_OPT_DIR = $(TARGET_DIR)/opt/geronimo_api/

define GERONIMO_API_INSTALL_TARGET_CMDS
	# Créer le dossier /opt/my_flask_app sur la cible
	mkdir $(GERONIMO_API_OPT_DIR)
	cp -R $(@D)/* $(GERONIMO_API_OPT_DIR)
	# Copier les fichiers Python dans /opt/my_flask_app
	# Créer le fichier service systemd
	install -D -m 0644 $(@D)/geronimo.service $(TARGET_DIR)/etc/systemd/system
endef

$(eval $(generic-package))
