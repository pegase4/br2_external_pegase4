################################################################################
#
# pegase-driver-module
#
################################################################################

PEGASE_DRIVER_VERSION     = v1.9
#PEGASE_DRIVER_SITE        = $(TOPDIR)/../../paquet/pegase-driver
#PEGASE_DRIVER_SITE_METHOD = local
PEGASE_DRIVER_SITE        = git@gitlab.univ-eiffel.fr:pegase4/pegase-driver.git
PEGASE_DRIVER_SITE_METHOD = git
PEGASE_DRIVER_INSTALL_STAGING = YES
PEGASE_DRIVER_DEPENDENCIES += linux
PEGASE_DRIVER_HEADERS = \
	pegase-hf8ch/ifsttar_hf8ch.h \
	pegase-g8ch/ifsttar_g8ch.h \
	pegase-sync/ifsttar_sync.h \
	pegase-sync/ifsttar-GpsData.h

define PEGASE_DRIVER_INSTALL_STAGING_CMDS
	$(foreach header,$(PEGASE_DRIVER_HEADERS),
		$(INSTALL) -D -m 0644 $(@D)/$(header) $(STAGING_DIR)/usr/include/$(notdir $(header))
	)
endef

define PEGASE_DRIVER_INSTALL_TARGET_CMDS
	$(foreach subdir,$(PEGASE_DRIVER_MODULE_SUBDIRS),
		$(TARGET_CONFIGURE_OPTS) $(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/$(subdir) \
		DESTDIR=$(TARGET_DIR) install
	)
	
endef

PEGASE_DRIVER_MODULE_SUBDIRS = \
	pegase-hf8ch \
	pegase-sync \
	pegase-g8ch

$(eval $(kernel-module))
$(eval $(generic-package))
