################################################################################
#
# GERONIMO_SCREEN
#
################################################################################

GERONIMO_SCREEN_VERSION     = v1.4
#GERONIMO_SCREEN_SITE        = $(TOPDIR)/../../paquet/geronimo-screen
#GERONIMO_SCREEN_SITE_METHOD = local
GERONIMO_SCREEN_SITE        = git@gitlab.univ-eiffel.fr:pegase4/geronimo-screen.git
GERONIMO_SCREEN_SITE_METHOD  = git
GERONIMO_SCREEN_INSTALL_STAGING = YES
GERONIMO_SCREEN_DEPENDENCIES += libpegase

$(eval $(qmake-package))
