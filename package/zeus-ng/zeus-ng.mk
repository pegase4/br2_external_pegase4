################################################################################
#
# zeus-ng
#
################################################################################

ZEUS_NG_VERSION     = v1.1
#ZEUS_NG_SITE        = $(TOPDIR)/../temp/zeus-ng
#ZEUS_NG_SITE_METHOD = local
ZEUS_NG_SITE        = git@gitlab.univ-eiffel.fr:pegase4/zeus-ng.git
ZEUS_NG_SITE_METHOD  = git
ZEUS_NG_INSTALL_STAGING = YES

ifeq ($(BR2_PACKAGE_ZEUS_TIME),y)
ZEUS_NG_CONF_ENV += ZEUS_TIME_ENABLE=true
else
ZEUS_NG_CONF_ENV += ZEUS_TIME_ENABLE=false
endif

ifeq ($(BR2_PACKAGE_ZEUS_UPLOAD),y)
ZEUS_NG_CONF_ENV += ZEUS_UPLOAD_ENABLE=true
else
ZEUS_NG_CONF_ENV += ZEUS_UPLOAD_ENABLE=false
endif

ifeq ($(BR2_PACKAGE_ZEUS_APPS),y)
ZEUS_NG_CONF_ENV += ZEUS_APPS_ENABLE=true
else
ZEUS_NG_CONF_ENV += ZEUS_APPS_ENABLE=false
endif

ifeq ($(BR2_PACKAGE_ZEUS_LOG),y)
ZEUS_NG_CONF_ENV += ZEUS_LOG_ENABLE=true
else
ZEUS_NG_CONF_ENV += ZEUS_LOG_ENABLE=false
endif

$(eval $(qmake-package))
